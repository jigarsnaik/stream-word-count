package com.jigarnaik;

import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.stream.LongStream;

public class Producer {

    public static void main(String[] args) {
        Producer producer = new Producer();
        producer.send();
    }



    public void send() {
        Properties config = new Properties();
        config.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        KafkaProducer<String, String> producer = new KafkaProducer<String, String>(config);
        LongStream.range(0, 2000).forEachOrdered(i -> {
            System.out.println("i = " + i);
            ProducerRecord<String, String> producerRecord = new ProducerRecord<>("test", "MyKey" + i,
                    "This is another type of message " + i);
            producer.send(producerRecord, (metadata, e) -> {
                if (e == null) {
                    System.out.println("Offset : " + metadata.offset() + ", partition : " + metadata.partition());
                } else {
                    e.printStackTrace();
                }
            });
        });


        producer.close();
    }
}

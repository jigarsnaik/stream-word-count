package com.jigarnaik.store;

import com.jigarnaik.WorldCountProcessor;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.StoreBuilder;
import org.apache.kafka.streams.state.Stores;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class StoreMain {

    final static Serde<String> stringSerde = Serdes.String();
    final static Serde<Long> longSerde = Serdes.Long();
    private static final Logger LOGGER = LoggerFactory.getLogger(WorldCountProcessor.class);

    public static void main(String[] args) {
        Properties properties = new Properties();
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "key-value-store-example");
        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        properties.put(StreamsConfig.STATE_DIR_CONFIG, "/Users/jigarnaik/Documents/workspace/kafka-word-count/store");
        String storeName = "event-store";
        StreamsBuilder builder = new StreamsBuilder();

        final StoreBuilder<KeyValueStore<String, String>> eventStore = Stores
                .keyValueStoreBuilder(
                        Stores.persistentKeyValueStore(storeName),
                        Serdes.String(),
                        Serdes.String())
                .withCachingEnabled();

        builder.addStateStore(eventStore);

        builder.stream("source-topic", Consumed.with(stringSerde, stringSerde))
                .peek((key, value) -> {
                    System.out.println("Key 1 : " + key + " , Value 1 : " + value);
                }).transform(new EventStoreTransformerSupplier(eventStore.name()), eventStore.name())
                .peek((key, value) -> {
                    System.out.println("Key 2 : " + key + " , Value : 2 " + value);
                }).to("target-topic");

        KafkaStreams kafkaStreams = new KafkaStreams(builder.build(), properties);
        kafkaStreams.start();

        LOGGER.info("Starting Stream");
        LOGGER.info(kafkaStreams.toString());

        Runtime.getRuntime().addShutdownHook(new Thread(kafkaStreams::close));
    }
}

package com.jigarnaik.store;


import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.kstream.TransformerSupplier;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.state.KeyValueStore;

public class EventStateStore implements Transformer<String, String, KeyValue<String, String>> {

    private final String storeName;
    private KeyValueStore<String, String> store;

    EventStateStore(String storeName) {
        this.storeName = storeName;
    }

    @Override
    public void init(ProcessorContext context) {
        System.out.println("Init started");
        store = (KeyValueStore<String, String>) context.getStateStore(storeName);
        System.out.println("Init completed");
    }

    @Override
    public KeyValue<String, String> transform(String key, String value) {
        System.out.println("Inside store - Key : " + key + " , Value : " + value);

        if (key.startsWith("Subscription") || key.startsWith("Redemption")) {
            String lookupKey = key.replaceAll("Subscription_", "").replaceAll("Redemption_", "");
            System.out.println("lookupKey : " + lookupKey);
            if (store.get(lookupKey) != null) {
                String otherLeg = store.get(lookupKey);
                value = value + otherLeg;
                store.delete(lookupKey);
                System.out.println(store.persistent());
                return KeyValue.pair(key, value);

            } else {
                store.put(lookupKey, value);
                return null;
            }
        } else {
            return KeyValue.pair(key, value + " Appended");
        }
    }

    @Override
    public void close() {
        System.out.println("Close called.");
    }
}

class EventStoreTransformerSupplier implements TransformerSupplier<String, String, KeyValue<String, String>> {

    private final String storeName;

    public EventStoreTransformerSupplier(String storeName) {
        this.storeName = storeName;
    }

    @Override
    public Transformer<String, String, KeyValue<String, String>> get() {
        return new EventStateStore(this.storeName);
    }
}

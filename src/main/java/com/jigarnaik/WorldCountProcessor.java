package com.jigarnaik;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Produced;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Properties;

public class WorldCountProcessor {

    final static Serde<String> stringSerde = Serdes.String();
    final static Serde<Long> longSerde = Serdes.Long();
    private static final Logger LOGGER = LoggerFactory.getLogger(WorldCountProcessor.class);

    public static void main(String[] args) {
        Properties properties = new Properties();
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "word-count");
        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());


        StreamsBuilder builder = new StreamsBuilder();
        KStream<String, String> textLines = builder.stream("word-count-input", Consumed.with(stringSerde, stringSerde));

        KTable<String, Long> wordCount = textLines.mapValues(val -> val.toLowerCase())
                .flatMapValues(value -> Arrays.asList(value.toLowerCase().split("\\W+")))
                .peek((key, val) -> {
                    System.out.println("key1= " + key + ", value1 = " + val);
                })
                .selectKey((key, val) -> val)
                .peek((key, val) -> {
                    System.out.println("key2= " + key + ", value2 = " + val);
                })
                .groupByKey()
                .count();

        wordCount.toStream().peek((key, val) -> {
            System.out.println("key3= " + key + ", value3 = " + val);
        }).to("word-count-output", Produced.with(stringSerde, longSerde));

        KafkaStreams kafkaStreams = new KafkaStreams(builder.build(), properties);
        kafkaStreams.start();

        LOGGER.info("Starting Stream");
        LOGGER.info(kafkaStreams.toString());

        Runtime.getRuntime().addShutdownHook(new Thread(kafkaStreams::close));
    }
}

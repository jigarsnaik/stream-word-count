#Usage
 Count words in line using kafka kstream and ktable.
##Create Topic
````
kafka-topics \
    --create \
    --zookeeper localhost:2181 \
    --replication-factor 1 \
    --partitions 1 \
    --topic word-count-input

kafka-topics \
    --create \
    --zookeeper localhost:2181 \
    --replication-factor 1 \
    --partitions 1 \
    --topic word-count-output
````
##List Topics 
````
kafka-topics \
    --list \
    --zookeeper localhost:2181
````
##Delete Topic
````
kafka-topics \
    --delete \
    --zookeeper localhost:2181 \
    --topic test
````
##Send a message to topic 
````
kafka-console-producer \
    --topic my-topic \
    --broker-list localhost:9092
````
##Read a message from topic
````
kafka-console-consumer \
    --topic my-topic \
    --from-beginning \
    --bootstrap-server localhost:9092

kafka-console-consumer --bootstrap-server localhost:9092 \
        --topic word-count-output \
        --from-beginning \
        --formatter kafka.tools.DefaultMessageFormatter \
        --property print.key=true \
        --property key.deserializer=org.apache.kafka.common.serialization.StringDeserializer \
        --property value.deserializer=org.apache.kafka.common.serialization.LongDeserializer
````   